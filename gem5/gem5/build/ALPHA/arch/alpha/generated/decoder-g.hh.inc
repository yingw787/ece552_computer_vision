// DO NOT EDIT
// This file was automatically generated from an ISA description:
//   alpha/isa/main.isa

#include "base/bitfield.hh"

#include <iomanip>
#include <iostream>
#include <sstream>

#include "arch/alpha/faults.hh"
#include "arch/alpha/types.hh"
#include "config/ss_compatible_fp.hh"
#include "cpu/static_inst.hh"
#include "mem/packet.hh"
#include "mem/request.hh"  // some constructors use MemReq flags
